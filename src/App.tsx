

import React from 'react';
import {
  FlatList,
  Pressable,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';

import { currencyByRupee } from './constants';
import CurrencyButton from './components/CurrencyButton';
import Snackbar from 'react-native-snackbar';



function App(): React.JSX.Element {

  const [inputValue, setInputValue] = React.useState('');
  const [resultValue, setResultValue] = React.useState('');
  const[targetCurrency,setTargetCurrency]=React.useState('');

  const buttonPressed =(targetValue:Currency)=> {
    if(!inputValue){
      return Snackbar.show({
        text:"Enter a value to convert",
        backgroundColor:"#EA7773",
        textColor:"#000000"
      })
    }
    const inputAmount = parseFloat(inputValue);
    if(!isNaN(inputAmount)){
      const convertedValue =  inputAmount * targetValue.value;
      const result = `${targetValue.symbol} ${convertedValue.toFixed(2)}`
      
      setResultValue(result)
      setTargetCurrency(targetValue.name)
    }
  else{
    return Snackbar.show({
      text:"Not a valid number to convert",
      backgroundColor:"#F4BE2C",
      textColor:"#000000"
    })

  }}



  return (
    <>
      <StatusBar/>
      <View style={styles.container}>
        <View>
        <View style={styles.rupeesContainer}>
          <Text style={styles.rupee} >₹</Text>
          <TextInput style={styles.inputValueText}
          maxLength={14}
          value={inputValue}
          clearButtonMode='always'
          onChangeText={setInputValue}
          keyboardType='numeric' 
          placeholder='Enter amount in rupees'
          />
          </View>
          {resultValue && (
            <Text style={styles.resultTxt}>
              {resultValue}
            </Text>
          )}
        </View>
        <View style={styles.bottomContainer}>
          <FlatList
          numColumns={3}
          data={currencyByRupee}
          keyExtractor={item=>item.name}
          renderItem={({item})=>(
            <Pressable
            style={[styles.button,targetCurrency===item.name && styles.selected]}
            onPress={()=> buttonPressed(item)}>
            <CurrencyButton {...item} />
            </Pressable>
          )}/>
        </View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#515151',
  },
  topContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  inputValueText:{
    color: '#fff',
    fontSize:20,
  },
  resultTxt: {
    fontSize: 32,
    color: '#ffffff',
    fontWeight: '800',
  },
  rupee: {
    marginRight: 8,
    fontSize: 22,
    color: '#ffffff',
    fontWeight: '800',
  },
  rupeesContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 4,
  },
  inputAmountField: {
    height: 40,
    width: 200,
    padding: 8,
    borderWidth: 1,
    borderRadius: 4,
    backgroundColor: '#FFFFFF',
  },
  bottomContainer: {
    flex: 3,
    padding:16
  },
  button: {
    flex: 1,
    margin: 12,
    height: 60,
    padding:10,
    borderRadius: 12,
    backgroundColor: '#fff',
    elevation: 2,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowColor: '#333',
    shadowOpacity: 0.1,
    shadowRadius: 1,
  },
  selected: {
    backgroundColor: '#ffeaa7',
  },
});

export default App;
